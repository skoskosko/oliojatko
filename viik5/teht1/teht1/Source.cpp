#include <iostream>
#include <string>
#include <set>

void main() {

	std::string input;
	std::getline(std::cin, input);
	std::set<char> output;
	for (char chr : input) { output.insert(chr);}

	for (char chr : output) {std::cout << chr << std::endl;}
	
	system("pause");
}