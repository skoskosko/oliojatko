#include <iostream>
#include <fstream>
#include <string>
#include <set>

void main() {

	std::multiset<std::string> words;
	std::set<std::string> wordsonce;
	std::ifstream fileInput;
	std::string temp;
	fileInput.open("input.txt");
	while (std::getline(fileInput, temp, ' ')) {
		words.insert(temp);
	}
	for (std::string s : words) {
		if (wordsonce.find(s) == wordsonce.end()) {
			wordsonce.insert(s);
			std::cout << "'" + s + "' : x " << words.count(s) << std::endl;
		}
	}
	fileInput.close();
	system("pause");
}