#include <iostream>
#include <fstream>
#include <string>
#include <map>

void main() {

	std::map<std::string, int> wordmap;
	std::ifstream fileInput;
	std::string word;
	fileInput.open("input.txt");
	while (std::getline(fileInput, word, ' ')) {
		if (wordmap.find(word) == wordmap.end()) {
			wordmap[word] = 1;
		}
		else {
			wordmap[word]++;
		}
	}
	for (auto i = wordmap.begin(); i != wordmap.end(); i++) {
		std::cout << i->first << " : " << i->second << " x" << std::endl;
	}
	fileInput.close();
	system("pause");
}