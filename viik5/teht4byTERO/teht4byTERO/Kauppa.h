#pragma once
#ifndef KAUPPA_H
#define KAUPPA_H
#include <string>
using std::string;
#include <iostream>
#include <set>
using std::ostream;
using std::istream;
class Kauppa
{
public:
	Kauppa();
	Kauppa(string kello, double hinta, int maara, string ostaja, string myyja);
	~Kauppa();
	std::string getMyyja() {
		return myyja_;
	}
	double getHinta() {
		return hinta_;
	}
	int getMaara() {
		return maara_;
	}
	friend ostream & operator<<(ostream & out, const Kauppa &k);
	friend istream & operator>>(istream & in, Kauppa &k);
	bool operator <(const Kauppa & k) const;
	inline void Kauppa::operator()(const Kauppa k, std::string input) const {
		int kauppojenlkm = 0;
		int summa = 0;


		if (k.myyja_ == input) {
			kauppojenlkm++;
			summa += k.maara_;
			std::cout << "j";
		}

	}
private:
	string aika_;
	double hinta_;
	int maara_;
	string ostaja_;
	string myyja_;

};
#endif