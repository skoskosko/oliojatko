#include <iostream>

using namespace std;

int main() {
//	int m = 0;
//	int n = 0;
//	[&, n](int a) mutable {m = ++n + a; } (4);
//	cout << m << endl << n << endl;


	auto n = [](int x, int y) {return x + y; }(5, 4);
	cout << n << endl;


	system("pause");
	return 0;
	
}