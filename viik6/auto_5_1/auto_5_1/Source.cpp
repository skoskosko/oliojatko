#include <iostream>
#include <string>
#include <set>

void main() {

	std::string input;
	std::getline(std::cin, input);
	std::set<char> output;
	for (auto chr : input) { output.insert(chr); }

	for (auto chr : output) { std::cout << chr << std::endl; }

	system("pause");
}