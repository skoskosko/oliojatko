#ifndef KAUPPA_H
#define KAUPPA_H
#include <string>
using std::string;
#include <iostream>
#include <sstream>
using std::ostream;
using std::istream;
using std::stringstream;
class Kauppa
{
public:
	Kauppa() {};
	Kauppa(string kello, double hinta, int maara, string ostaja,
		string myyja) : aika_(kello), hinta_(hinta),
		maara_(maara), ostaja_(ostaja), myyja_(myyja) {};

	~Kauppa(){};
	friend ostream & operator<<(ostream & out, const Kauppa &k) {
		return out << 
			"Kello: " << k.aika_ << std::endl << 
			"Myyja: " << k.myyja_ << std::endl << 
			"Hinta: " << k.hinta_ << std::endl << 
			"Ostaja: " << k.ostaja_ << std::endl << 
			"M��r�: " << k.maara_ << std::endl;
	};
	friend istream & operator>>(istream & in, Kauppa &k) {
		return in >> k.aika_ >> k.hinta_ >> k.maara_ >> k.myyja_ >> k.ostaja_;
	};
	bool operator <(const Kauppa & k) const {
		return this->hinta_ > k.hinta_;
	};
private:
	string aika_;
	double hinta_;
	int maara_;
	string ostaja_;
	string myyja_;
};
#endif



#include <iterator>
#include <vector>
using std::vector;
#include <string>
using std::string;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include <iostream>
using std::ostream_iterator;
using std::istream_iterator;
using std::cout;
using std::cerr;
using std::endl;
#include <algorithm>

int main()
{
	string poisto;
	vector<Kauppa> V;
	ifstream tied("nokia18032009.txt");
	ofstream tulos("harjoitus5t1.txt");
	
	if (tied.is_open())
	{
		getline(tied, poisto); // luetaan otsikkorivi pois
							   // luetaan tiedostosta tiedot Kauppa-oliohin vektoriin V
		
		copy(istream_iterator<Kauppa>(tied), istream_iterator<Kauppa>(),
			back_inserter(V));
		sort(V.begin(), V.end()); // j�rjestell��n vektori hinnan mukaan
		if (tulos.is_open())
			// tulostetaan j�rjestetty vektori tiedostoon
			copy(V.begin(), V.end(), ostream_iterator<Kauppa>(tulos, "\n"));
		else
		{
			cerr << "tiedoston harjoitus5t1.txt avaus ei onnistunut" << endl;
			return 2;
		}
	}
	else
	{
		cerr << "tiedoston nokia18032009.txt avaus ei onnistunut" << endl;
		return 1;
	}
	
	return 0;
}
