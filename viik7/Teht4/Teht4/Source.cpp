#ifndef KAUPPA_H
#define KAUPPA_H
#include <string>
using std::string;
#include <iostream>
#include <sstream>
using std::ostream;
using std::istream;
using std::stringstream;
class Kauppa
{
public:
	Kauppa() {};
	Kauppa(string kello, double hinta, int maara, string ostaja,
		string myyja) : aika_(kello), hinta_(hinta),
		maara_(maara), ostaja_(ostaja), myyja_(myyja) {};

	~Kauppa() {};
	string myyja() {
		return myyja_;
	};
	double hinta() {
		return hinta_;
	}
	double maara() {
		return maara_;
	}
	friend ostream & operator<<(ostream & out, const Kauppa &k) {
		return out <<
			"Kello: " << k.aika_ << std::endl <<
			"Myyja: " << k.myyja_ << std::endl <<
			"Hinta: " << k.hinta_ << std::endl <<
			"Ostaja: " << k.ostaja_ << std::endl <<
			"M��r�: " << k.maara_ << std::endl;
	};
	friend istream & operator>>(istream & in, Kauppa &k) {
		return in >> k.aika_ >> k.hinta_ >> k.maara_ >> k.myyja_ >> k.ostaja_;
	};
	bool operator <(const Kauppa & k) const {
		return this->hinta_ > k.hinta_;
	};
	bool Kauppa::onsama() {
		if (ostaja_ == myyja_) {
			return true;
		}
		else {
			return false;
		}
	};


private:
	string aika_;
	double hinta_;
	int maara_;
	string ostaja_;
	string myyja_;
};
#endif



#ifndef YKSILO_H
#define YKSILO_H
#include <string>
using std::string;
#include <iostream>
#include <sstream>
using std::ostream;
using std::istream;
using std::stringstream;
class Yksilo
{
public:
	Yksilo() {};
	Yksilo(string kuka, double arvo, int osakkeet) : kuka_(kuka), arvo_(arvo), osakkeet_(osakkeet) {
		maara_ = 0;
	};

	~Yksilo() {};
	friend ostream & operator<<(ostream & out, const Yksilo &k) {
		return out <<
			"Kuka: " << k.kuka_ << std::endl <<
			"arvo: " << k.arvo_ << std::endl <<
			"osakemaara: " << k.osakkeet_ << std::endl <<
			"montako: " << k.maara_ << std::endl;
	};
	friend istream & operator>>(istream & in, Yksilo &k) {
		return in >> k.maara_;
	};
	bool operator <(const Yksilo & k) const {
		return this->maara_ > k.maara_;
	};
	void operator ++(int) {
		maara_++;
	};
	void operator +=(double asd) {
		arvo_ += asd;
	};
	void operator +=(int asd) {
		osakkeet_ += asd;
	};
	bool Yksilo::operator==(const Yksilo & uusi) const
	{
		return this->kuka_ == uusi.kuka_;
	};


private:
	string kuka_;
	int maara_;
	int osakkeet_;
	double arvo_;
};
#endif






#include <iterator>
#include <vector>
using std::vector;
#include <string>
using std::string;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include <iostream>
using std::ostream_iterator;
using std::istream_iterator;
using std::cout;
using std::cerr;
using std::endl;
#include <algorithm>

int main()
{
	string poisto;
	vector<Kauppa> V;
	vector<Yksilo> F;
	ifstream tied("nokia18032009.txt");

	if (tied.is_open())
	{
		getline(tied, poisto); // luetaan otsikkorivi pois
							   // luetaan tiedostosta tiedot Kauppa-oliohin vektoriin V
		copy(istream_iterator<Kauppa>(tied), istream_iterator<Kauppa>(),
			back_inserter(V));
	}
	else
	{
		cerr << "tiedoston nokia18032009.txt avaus ei onnistunut" << endl;
		return 1;
	}




	vector<Yksilo> damn = [](vector<Yksilo> b, vector<Kauppa> c) mutable {// katso onko joku jossain, jos ei niin lis�� jos on lis�� siihen yksi
		for (auto value : c) {

			Yksilo temp = Yksilo(value.myyja(), value.hinta(), value.maara());
			ptrdiff_t pos = find(b.begin(), b.end(), temp) - b.begin();

			if (pos >= b.size()) {
				// ei ollu
				temp++;
				b.push_back(temp);
			}
			else {
				// oli
				b[pos] += value.hinta();
				b[pos] += value.maara();
				b[pos]++;
			}




		}
		return b;
	} (F, V);


	ofstream tulos("harjoitus74.txt");
	if (tulos.is_open())
		// tulostetaan j�rjestetty vektori tiedostoon
		copy(damn.begin(), damn.end(), ostream_iterator<Yksilo>(tulos, "\n"));
	else
	{
		cerr << "tiedoston harjoitus74.txt avaus ei onnistunut" << endl;
		return 2;
	}

	system("pause");
	return 0;
}
