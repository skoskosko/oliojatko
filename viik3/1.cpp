// Stack class template.
#ifndef TSTACK1_H
#define TSTACK1_H
template< typename T, int SIZE = 10>
class Stack
{
public:
	Stack(); // default constructor (stack size 10)
					 // destructor
	~Stack()
	{
		delete[] stackPtr;
	} // end ~Stack destructor
	bool push(const T&); // push an element onto the stack
	bool pop(T&); // pop an element off the stack
				  // determine whether Stack is empty
	bool isEmpty() const
	{
		return top == -1;
	} // end function isEmpty
	  // determine whether Stack is full
	bool isFull() const
	{
		return top == size - 1;
	} // end function isFull
private:
	int size; // # of elements in the stack
	int top; // location of the top element
	T *stackPtr; // pointer to the stack
}; // end class Stack
   // constructor




template< typename T, int SIZE>
Stack< T , SIZE>::Stack()
{
	size = SIZE > 0 ? SIZE : 10;
	top = -1; // Stack initially empty
	stackPtr = new T[size]; // allocate memory for elements
} // end Stack constructor

  // push element onto stack;
  // if successful, return true; otherwise, return false


template< typename T, int SIZE>
bool Stack< T, SIZE >::push(const T &pushValue)
{
	if (!isFull())
	{
		stackPtr[++top] = pushValue; // place item on Stack
		return true; // push successful
	} // end if
	return false; // push unsuccessful
} // end function push
  // pop element off stack;
  // if successful, return true; otherwise, return false


template< typename T, int SIZE>
bool Stack< T ,SIZE>::pop(T &popValue)
{
	if (!isEmpty())
	{
			popValue = stackPtr[top--]; // remove item from Stack
		return true; // pop successful
	} // end if
	return false; // pop unsuccessful
} // end function pop
#endif



// Stack class template test program. Function main uses a
// function template to manipulate objects of type Stack< T >.
#include <iostream>
using std::cout;
using std::cin;
using std::endl;
//#include "tstack1.h" // Stack class template definition
// function template to manipulate Stack< T >




template< typename T , int SIZE>
void testStack
(
	Stack< T , SIZE> &theStack, // reference to Stack< T >
	T value, // initial value to push
	T increment, // increment for subsequent values
	const char *stackName) // name of the Stack < T > object
{
	cout << "\nPushing elements onto " << stackName << '\n';
	while (theStack.push(value))
	{
		cout << value << ' ';
		value += increment;
	} // end while
	cout << "\nStack is full. Cannot push " << value
		<< "\n\nPopping elements from " << stackName << '\n';
	while (theStack.pop(value))
		cout << value << ' ';
	cout << "\nStack is empty. Cannot pop\n";
} // end function testStack




int main()
{
	Stack< double, 5> doubleStack;
	Stack< int > intStack;
	testStack(doubleStack, 1.1, 1.1, "doubleStack");
	testStack(intStack, 1, 1, "intStack");
	system("pause");
	return 0;
} // end main