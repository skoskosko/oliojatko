#include <iostream>
#include <string>

using std::string;
using std::cout;

namespace test {
		int max(const int eka, const int toka) {
			if (eka > toka)
				return eka;
			return toka;
		}
		float max(const float eka, const float toka) {
			if (eka > toka)
				return eka;
			return toka;
		}
		string max(const string eka, const string toka) {
			if (eka > toka)
				return eka;
			return toka;
		}
}

using test::max;

int main() {
	const int a1 = 4;
	const int a2 = 2;
	cout << max(a1, a2);
	const float b1 = 4;
	const float b2 = 2;
	cout << max(b1, b2);
	const string c1 = "aaasd";
	const string c2 = "baasd";
	cout << max(c1, c2);
	system("pause");
	return 0;
}