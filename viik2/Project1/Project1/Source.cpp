#include <iostream>
#include <string>


using std::string;
using std::cout;
using std::endl;
class henkilo
{
private:
	double iLoan;
	string enimi;
	string snimi;
	string sotu;


	string iCustomer;
public:
	henkilo(string aCustomer) :iLoan(0), iCustomer(aCustomer)
	{}
	void SetLoan(double aLoan)
	{
		iLoan = aLoan;
	}
	double GetLoan()
	{
		return iLoan;
	}
	void SetNimi(string nimi)
	{
		string delimiter = " ";
		enimi = nimi.substr(0, nimi.find(delimiter));
		snimi = nimi.substr(nimi.find(delimiter), nimi.length());
	}
	string GetNimi()
	{
		return enimi + " " + snimi;
	}
};
/*Alykäs osoitin varsinaisen osoittimen hallintaa varten*/
class SMART_ptr
{
public:
	SMART_ptr(string aCustomer);
	~SMART_ptr();
	henkilo &operator*()const;
	henkilo *operator->()const;
private:
	void Load()const;
	string iCustomer;
	mutable henkilo *iS; //Dynamic resource
};
SMART_ptr::~SMART_ptr()
{
	if (!iS) //Varmistetaan resurssin vapauttaminen
		delete iS;
}

SMART_ptr::SMART_ptr(string aNumber)
	:iCustomer(aNumber), iS(0)
{
}
void SMART_ptr::Load()const {
	if (!iS)
		iS = new henkilo(iCustomer);
}
henkilo &SMART_ptr::operator*()const {
	Load(); //Testataan aina onko osoitin validi
	return *iS;
}
henkilo *SMART_ptr::operator->()const {
	Load(); //Testataan aina onko osoitin validi
	return iS;
}



/*Testipääohjelma*/
int main()
{
	SMART_ptr Harray[2] = { { "TOIMI" },{ "KOODI" } };

	SMART_ptr Customer1("Donald Duck"); //smart-osoitin
	SMART_ptr Customer2("Donald Duck"); //smart-osoitin
	Harray[0] = Customer1;
	Harray[1] = Customer2;


	Harray[0]->SetNimi("Martti Ahtisaari");
	//Customer1->SetLoan(475); //Lainan asetus smart-osoittimen avulla
	cout << "Nimi value: " << Harray[0]->GetNimi() << endl;
	return 0;
}
