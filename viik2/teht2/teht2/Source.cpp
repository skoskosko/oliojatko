#include <cstdlib>
#include <iostream>

#include <string>
using namespace std;



class City {
private:
	class Data
	{
	public:
		Data(string paikkakunta = "") : iName(paikkakunta), iCount(1)
		{
		}
		const string &Name(void) const
		{
			return iName;
		}
		long Count()
		{
			return iCount;
		}
		long operator++()
		{
			iCount = iCount + 1;
			//++iCount;
			return iCount;
		}
		long operator--()
		{
			iCount = iCount - 1;
			//--iCount;
			return iCount;
		}
	private:
		string iName;
		long iCount;
	};

	Data* iData;

public:
	City()
	{
		iData = new Data;
	}
	City(const string &name)
	{	
		iData = new Data(name);
	}
	City(City &city)
	{
		iData = new Data;
		city.iData->operator++();
		*iData = *city.iData;
		
		// one reference more
	}
	~City()
	{
		// one reference less
		iData->operator--();
		delete iData;
	}
	City &operator=(City &city)
	{
		if (this == &city)
			return *this;
		
		iData = city.iData;
		iData->operator++();
		return *this;
	}
	const string &Name() const
	{
		return iData->Name();
	}
};



int main()
{
	City c1("TAMPERE");//creation
	City c2(c1);//copy constructor
	City c3; //default constructor
	cout << "Assignment" << endl;
	c3 = c1;
	cout << "End" << endl;
	system("PAUSE");
	return EXIT_SUCCESS;
}


