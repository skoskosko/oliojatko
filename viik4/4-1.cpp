// basic file operations
#include <iostream>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

int main() {
	std::vector<std::string> vs;
	std::string line;
	std::ifstream myfile("input.txt");
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			vs.push_back(line);
		}
		myfile.close();
	}

	std::sort(vs.begin(), vs.end());

	std::ofstream outfile("output.txt");
	if (outfile.is_open())
	{
		for (int i = 0; i < vs.size(); i++) {
			std::string temp = vs[i] + "\n";
			outfile << temp;
		
		}
		outfile.close();
	}
	
	


	return 0;
}