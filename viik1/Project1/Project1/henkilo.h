#include<string>
#pragma once
class henkilo
{
public:
	henkilo();
	henkilo(std::string enimi, std::string snimi, std::string sotu);
	~henkilo();
	henkilo& operator = (const std::string nimi);
	operator std::string();
	void setENimi(std::string nimi) const;


private:
	std::string sukunimi_;
	mutable std::string etunimi_;
	std::string sotu_;

};
