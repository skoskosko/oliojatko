#include "henkilo.h"
#include <string>
#include <iostream>




henkilo::henkilo()
{
}

henkilo::henkilo(std::string enimi, std::string snimi, std::string sotu)
{
	etunimi_ = enimi;
	sukunimi_ = snimi;
	sotu_ = sotu;
}


henkilo::~henkilo()
{
}

henkilo & henkilo::operator=(const std::string nimi)
{
	std::string delimiter = " ";
	etunimi_ = nimi.substr(0, nimi.find(delimiter));
	sukunimi_ = nimi.substr(nimi.find(delimiter), nimi.length());
	return *this;
}

henkilo::operator std::string()
{
	return etunimi_ + " " + sukunimi_;
}

void henkilo::setENimi(std::string nimi) const
{
	etunimi_ = nimi;
}



